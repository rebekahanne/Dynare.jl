# Put Dynare.jl files in the path.
pushfirst!(LOAD_PATH, abspath("../../src"))

using Dynare
using SteadyState

# Preprocess the modfile
@dynare "rbc_j.mod"

model = rbc_j.model_
oo = rbc_j.oo_

yinit = [0.4193967, 0.3764809, 0.3538641, 0.4032740, 0.9500000, 0, 3.051005982347480*1.0e+08]

@time steady!(model, oo, yinit)
