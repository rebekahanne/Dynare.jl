using BinaryProvider

const verbose = "--verbose" in ARGS
const prefix = Prefix(get([a for a in ARGS if a != "--verbose"], 1, joinpath(@__DIR__, "usr")))

products = Product[
    ExecutableProduct(prefix, "dynare_m", :dynare),
]

PREPROCESSOR_VERSION = "d97b9c24"
REMOTE_PATH = "https://www.dynare.org/preprocessor/d97b9c24"

download_info = Dict(
    Linux(:i686, :glibc) => ("$REMOTE_PATH/linux/32/preprocessor.tar.gz", "f760cf16735fed80de08952893a13e4747dc33c7bf682433760222d7cb91fe7e"),
    Linux(:x86_64, :glibc) => ("$REMOTE_PATH/linux/64/preprocessor.tar.gz", "c8b69a09f6c02e034f44bf985d812ced4ab8c34b358909fe550260654acc5d54"),
    Windows(:i686) => ("$REMOTE_PATH/windows/32/preprocessor.tar.gz", "621e91388f5f2b926fda97635602a67b303e3562ff072d03c2daa8fb229966c4"),
    Windows(:x86_64) => ("$REMOTE_PATH/windows/64/preprocessor.tar.gz", "6c6aff2bde7ed7fcfe20eb60bea8bba212c36b99a14da94219c643571946464c"),
    MacOS() => ("$REMOTE_PATH/macOS/64/preprocessor.tar.gz", "6a8d61b997b3c3364afd884d6da87d894a0bcd59a0dee2261a70e77b0a17a232"),
 )

for p in products
    if platform_key() in keys(download_info)
        url, tarball_hash = download_info[platform_key()]
        install(url, tarball_hash; prefix=prefix, force=true, verbose=true, ignore_platform=true)
    else
        error("Your platform $(Sys.MACHINE) is not supported by Dynare.jl!")
    end
    write_deps_file(joinpath(@__DIR__, "deps.jl"), products; verbose=true)
end
